<?php 
session_start();
require "logica/Pelicula.php";
require "persistencia/conexion.php";
$pid = null;
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}
?>


<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <link href="https://bootswatch.com/4/darkly/bootstrap.css"
    rel="stylesheet" />
   

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" ></script>
    <script src="cookiealert-standalone.js"></script>

    <title>Cinema</title>
    <link rel="icon" type="image/png" href="img/logo.png">
   
  </head>
  <body>
    
  <?php
   if (isset($pid)) {
    include "presentacion/menupelicula.php";
        include $pid;
    }else{
      include "presentacion/menupelicula.php";
      include "presentacion/Bienvenido.php";
    }
   ?>
  
  
  
   
  

  </body>
</html>