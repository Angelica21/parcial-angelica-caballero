
<?php
require "persistencia/PeliculaDAO.php";

class Pelicula{
    private $idPelicula;
    private $titulo;
    private $genero;
    private $duracion;
    private $conexion;
    private $PeliculaDAO;

    
    /**
     * @return string
     */
    public function getIdPelicula()
    {
        return $this->idPelicula;
    }

    /**
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }
    public function getDuracion()
    {
        return $this->duracion;
    } 
    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

   

    function Pelicula ($pIdPelicula="", $pGenero="",$pDuracion="", $pTitulo="") {
        $this -> idPelicula = $pIdPelicula;
        $this -> genero = $pGenero;
        $this -> duracion = $pDuracion;
        $this -> titulo = $pTitulo;
        $this -> conexion = new conexion();
        $this -> PeliculaDAO = new PeliculaDAO($pIdPelicula, $pGenero,$pDuracion, $pTitulo);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> autorDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> PeliculaDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> autorDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $autores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($autores, new Autor($resultado[0], $resultado[1], $resultado[2]));
        }
        return $autores;
    }
    
    
   
    
    
}


?>