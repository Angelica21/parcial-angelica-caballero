<?php

$creado=false;
if(isset($_POST["crear"])){
	$pelicula= new Pelicula("",$_POST["genero"],$_POST["duracion"],$_POST["titulo"]) ;
	$pelicula->crear();
	$creado=true;
}

?>

<div class="container">
	<div class="row mt-3">
        <div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h3>Crear Pelicula</h3>
				</div>
				<div class="card-body">
				<?php
              if($creado==true){ ?>
				<div class="alert alert-primary alert-dismissible fade show"
							role="alert">
							<strong>Se han ingresado los datos</strong>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
				</div>
				<?php }?>

				<form action=<?php echo "index.php?pid=" .base64_encode("presentacion/Pelicula/crearPelicula.php")?> method="POST"><!-- Accion de guardar informacion--->
               <div class="form-group">
                 
                <input name="genero" type="text" class="form-control " placeholder="Genero" required="required">
               </div>
               <div class="form-group">
                 
                <input name="duracion" type="text" class="form-control " placeholder="Duracion" required="required">
               </div>
               <div class="form-group">
               
                 
                 <input type="text" name="titulo" class="form-control" placeholder="Titulo" required="required" >
               </div>
              
               <button type="submit" name ="crear" class="btn btn-primary">Crear</button>
             </form>
				</div>
			</div>
		</div>
	</div>
</div>

